package com.solis.arbolesdeclase.arbol;

/**
 *
 * @author solis
 */
public class ArbolBinario {

    NodoArbol raiz;

    public ArbolBinario() {
        raiz = null;
    }

    public void agregarNodo(int d, String nom) {
        NodoArbol nuevo = new NodoArbol(d, nom);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            NodoArbol auxiliar = raiz;
            NodoArbol padre;
            while (true) {
                padre = auxiliar;
                if (d < auxiliar.dato) {
                    auxiliar = auxiliar.hijoizquierdo;
                    if (auxiliar == null) {
                        padre.hijoizquierdo = nuevo;
                        return;
                    }
                } else {
                    auxiliar = auxiliar.hijoderecho;
                    if (auxiliar == null) {
                        padre.hijoderecho = nuevo;
                        return;
                    }
                }
            }
        }
    }

    public boolean estaVacio() {
        return raiz == null;
    }

    public void inOrden(NodoArbol r) {       
        if (r != null) {
            inOrden(r.hijoizquierdo);
            System.out.println(r.dato);
            inOrden(r.hijoderecho);
        }
    }

    public void preOrden(NodoArbol r) {    
        if (r != null) {
            System.out.println(r.dato);
            preOrden(r.hijoizquierdo);
            preOrden(r.hijoderecho);
        }
    }

    public void postOrden(NodoArbol r) {
        if (r != null) {          
            postOrden(r.hijoizquierdo);
            postOrden(r.hijoderecho);
            System.out.println(r.dato);
        }
    }
    public boolean eliminarNodo(int d){
NodoArbol aux = raiz;
NodoArbol padre = raiz;
boolean esHijoIzq = true;
while(aux.dato !=d){
padre = aux;
if (d<aux.dato){
esHijoIzq = true;
aux = aux.hijoizquierdo;
}else{
esHijoIzq = false;
aux = aux.hijoderecho;
}

if (aux == null){
return false;
}
} 
    if (aux.hijoizquierdo == null && aux.hijoderecho == null){

if (aux == raiz){
raiz = null;
}else if (esHijoIzq){ 
padre.hijoizquierdo = null;
}else{
padre.hijoderecho = null;
}
}else if(aux.hijoderecho == null){
if (aux==raiz){
raiz = aux.hijoizquierdo;
}else if (esHijoIzq){
padre.hijoizquierdo = aux.hijoderecho;
}else{
padre.hijoderecho = aux.hijoizquierdo;
}
}else{ 
NodoArbol nodoremplazo = obtenerNodoshijos(aux);
if(aux == raiz){
raiz = nodoremplazo;
}else if (esHijoIzq){
padre.hijoizquierdo = nodoremplazo;
}else{
padre.hijoderecho = nodoremplazo;
}
nodoremplazo.hijoizquierdo = aux.hijoizquierdo;
}
return true;
} 
    
    public NodoArbol obtenerNodoshijos (NodoArbol nodosHijos){
NodoArbol NodoPadre = nodosHijos;
NodoArbol temporal = nodosHijos;
NodoArbol auxiliar = nodosHijos.hijoderecho;
while (auxiliar != null){
NodoPadre = temporal;
temporal = auxiliar;
auxiliar = auxiliar.hijoizquierdo;
}
if (temporal != nodosHijos.hijoderecho){
NodoPadre.hijoizquierdo = temporal.hijoderecho;
temporal.hijoderecho = nodosHijos.hijoderecho;
}
System.out.println("Cambiando Nodo"+temporal.toString());
return temporal;
}
    public NodoArbol buscarNodo(int d){
        NodoArbol aux=raiz;
        while(aux.dato!=d){
        if(d<aux.dato){
            aux=aux.hijoizquierdo;
        }else{
        aux=aux.hijoderecho;}
        if(aux==null){
        return null;}
        }
        
        return aux;
    }
}
    

