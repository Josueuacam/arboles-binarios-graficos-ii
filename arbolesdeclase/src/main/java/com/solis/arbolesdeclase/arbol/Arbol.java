package com.solis.arbolesdeclase.arbol;

/**
 *
 * @author solis
 */
import javax.swing.JOptionPane;
public class Arbol {

    public static void main(String[] args) {
        int dato;
        String nombre;
        int opcion= 0, elemento;
        ArbolBinario miArbol = new ArbolBinario();
        do{
            try{
            opcion= Integer.parseInt(JOptionPane.showInputDialog(null,
              "1. Agregar un nodo\n"
            + "2. correr el arbol en inOrden\n"
            + "3. correr el arbol en preOrden\n"
            + "4. correr el arbol en postOrden\n"
            + "5. Buscar un nodo en el árbol\n"
            + "6. Eliminar un nodo\n"  
            + "7. Salir\n"
            + "Elige una opción...","Árboles binarios"
                    ,JOptionPane.QUESTION_MESSAGE));
            switch(opcion){
                case 1:
                    elemento = Integer.parseInt(JOptionPane.showInputDialog(null,
                            "Ingresa el número del nodo...","Agregando Nodo",
                            JOptionPane.QUESTION_MESSAGE));
                    nombre = JOptionPane.showInputDialog(null,
                            "Ingresa el nombre del nodo...","Agregando Nodo",
                            JOptionPane.QUESTION_MESSAGE);
                    miArbol.agregarNodo(elemento, nombre);
                    break;
                case 2:
                    if(!miArbol.estaVacio()){
                    System.out.println();
                    miArbol.inOrden(miArbol.raiz);
                    }else{
                    JOptionPane.showMessageDialog(null, "El arbol está vacío",
                            "Revise los datos ingresados",JOptionPane.INFORMATION_MESSAGE);
                    }
                    break;
                case 3:
                    if(!miArbol.estaVacio()){
                    System.out.println();
                    miArbol.preOrden(miArbol.raiz);
                    }else{
                    JOptionPane.showMessageDialog(null, "El arbol está vacío",
                            "Revise los datos ingresados",JOptionPane.INFORMATION_MESSAGE);
                    }
                    break;
                case 4:
                    if(!miArbol.estaVacio()){
                    System.out.println();
                    miArbol.postOrden(miArbol.raiz);
                    }else{
                    JOptionPane.showMessageDialog(null, "El arbol está vacío",
                            "Revise los datos ingresados",JOptionPane.INFORMATION_MESSAGE);
                    }
                    break;
                case 5:
                    if(!miArbol.estaVacio()){
                        elemento = Integer.parseInt(JOptionPane.showInputDialog(null,
                            "Ingresa el número del nodo buscado...","Buscando Nodo",
                            JOptionPane.QUESTION_MESSAGE));
                        
                        if(miArbol.buscarNodo(elemento)== null){
                        JOptionPane.showMessageDialog(null, "Nodo no encontrado",
                            "El nodo no parece estar en el árbol",JOptionPane.INFORMATION_MESSAGE);
                        }else{
                        System.out.println("Nodo encontrado: "+miArbol.buscarNodo(elemento));
                        }
                    }else{
                    JOptionPane.showMessageDialog(null, "El arbol está vacío",
                            "Revise los datos ingresados",JOptionPane.INFORMATION_MESSAGE);
                    }
                    break;
                case 6:
                    if(!miArbol.estaVacio()){
                        elemento = Integer.parseInt(JOptionPane.showInputDialog(null,
                            "Ingresa el número del a eliminar...","Eliminando Nodo",
                            JOptionPane.QUESTION_MESSAGE));
                        
                        if(miArbol.eliminarNodo(elemento)==false){
                        JOptionPane.showMessageDialog(null, "El Nodo no ha sido encontrado",
                            "El nodo no parece estar en el árbol",JOptionPane.INFORMATION_MESSAGE);
                        }else{
                        JOptionPane.showMessageDialog(null, "El Nodo ha sido eliminado",
                            "El nodo ya no está",JOptionPane.INFORMATION_MESSAGE);
                        }
                    }else{
                    JOptionPane.showMessageDialog(null, "El arbol está vacío",
                            "Revise los datos ingresados",JOptionPane.INFORMATION_MESSAGE);
                    }
                    break;
                case 7:
                    JOptionPane.showMessageDialog(null, "Fin de la ejecucion",
                            "Fin",JOptionPane.INFORMATION_MESSAGE);
                    break;
                default:
                 JOptionPane.showMessageDialog(null, "Opción incorrecta",
                            "Asegurese de ingresar bien los datos",JOptionPane.INFORMATION_MESSAGE);   
            }
            }catch(NumberFormatException n){
            JOptionPane.showMessageDialog(null, " elija una opcion " + n.getMessage());
            }
        }while(opcion!=7);
        System.out.println("InOrden");
        if (!miArbol.estaVacio()){
            miArbol.inOrden(miArbol.raiz);
        }
        System.out.println("PreOrden");
        if (!miArbol.estaVacio()){
            miArbol.preOrden(miArbol.raiz);
        }
        System.out.println("PostOrden");
        if (!miArbol.estaVacio()){
            miArbol.postOrden(miArbol.raiz);
        }
    }
    
}
